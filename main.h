void error_callback(int error, const char* description);
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
int initWindow(void);
void setupPoints(void);
int mainLoop(void);
void renderPoints();
void showPoint(float x, float y, float red, float green, float blue, int size);
void cleanup(void);
float float_rand( float min, float max );