#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <graphene.h>

#define WIDTH       800
#define HEIGHT      600
#define MAX_POINTS  100

GLFWwindow* window = NULL;
graphene_point_t *points[MAX_POINTS];

void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

int initWindow(void)
{
    if (!glfwInit())
    {
        printf("GLFW: Initialisation Failed\n");
        return -1;
    }

    glfwSetErrorCallback(error_callback);

    window = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL Window", NULL, NULL);
    if (!window)
    {
        fprintf(stderr, "Error: GLFW: Unable to create window\n");
        return -1;
    }

    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
      return -1;
    }
    fprintf(stdout, "Using GLEW %s\n", glewGetString(GLEW_VERSION));

    return 0;
}

void setupPoints(void)
{
    for (int i = 0; i<MAX_POINTS; i++)
    {
        points[i] = graphene_point_alloc ();
        graphene_point_init (points[i], float_rand(-1, 1), float_rand(-1, 1));
    }
}

int mainLoop(void)
{
    while(!glfwWindowShouldClose(window)) {
        glClearColor(0, 0, 0, 1.0f);
        // glClearColor(277, 277, 277, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT); 
        renderPoints();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    return 0;
}

void renderPoints()
{
    for (int i = 0; i<MAX_POINTS; i++)
    {
        showPoint(points[i]->x, points[i]->y, float_rand(-10, 255),float_rand(0, 255),float_rand(0, 255), float_rand(1,5));
    }
}

void showPoint(float x, float y, float red, float green, float blue, int size)
{
    glPointSize(size);
    glBegin(GL_POINTS);
      glColor3f(red, green, blue); 
      glVertex2f(x, y);
    glEnd();
}

void cleanup(void)
{
    glfwDestroyWindow(window);
    glfwTerminate();

    for (int i = 0; i<MAX_POINTS; i++)
    {
        graphene_point_free(points[i]);
    }
}

float float_rand( float min, float max )
{
    float scale = rand() / (float) RAND_MAX; /* [0, 1.0] */
    return min + scale * ( max - min );      /* [min, max] */
}

int main() {

    srand(time(NULL));

    printf("initWindow.\n");
    if(initWindow())
    {
        cleanup();
        return EXIT_FAILURE;
    }

    setupPoints();

    printf("mainLoop.\n");
    if(mainLoop())
    {
        cleanup();
        return EXIT_FAILURE;
    }

    printf("Final CleanUp.\n");
    cleanup();
    return EXIT_SUCCESS;
}

